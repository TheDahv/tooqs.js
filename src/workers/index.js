// @flow

import type {
  ConsumerError,
  PublisherFn,
  PublisherError,
  Worker,
  WorkerError,
} from '../tooqs'

const Doubler : Worker = (publish: PublisherFn) => num => {
  publish(num * 2);
}

const English : Worker = (publish: PublisherFn) => words =>
  publish('do what you done for knee high on a grasshopper');

const Evener : Worker = (publish: PublisherFn) => {
  return (nums: number[]) => {
    debugger;
    nums.
      filter(num => num % 2 === 0).
      forEach(msg => {
        debugger
        publish(msg)
      });

    return null;
  }
}

const Italian : Worker = (publish: PublisherFn) => words =>
  publish('È il mio cavallo di battaglia');

const Printer : Worker = (publish: PublisherFn) => (data: any) =>
  console.log(data);

const Sanitizer : Worker = (publish: PublisherFn) => words =>
  publish(
    words.
      replace(/[-,]/g, '').
      split(' ').
      map(word => word.toLowerCase()).
      join(' ')
  );

const Spanish : Worker = (publish: PublisherFn) => (words) =>
  publish('No me abra los ojos que no le voy a echar gotas');

module.exports = {
  Doubler,
  English,
  Evener,
  Italian,
  Printer,
  Sanitizer,
  Spanish
};
