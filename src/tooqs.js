/* @flow */

/**
 * pipeline is the starting point for working with tooqs.js.
 *
 * It returns an instance of Pipeline named for the given topic that you can
 * register workers with to build a pipeline.
 *
 * @param {string} topic - Names the pipeline and creates a public reference
 * point to submit work to the beginning of the pipeline.
 * @returns {Pipeline}
 */
function pipeline(topic: string) : Pipeline {
  return new Pipeline(topic);
}

/**
 * Message describes data flowing between pipeline workers.
 *
 * TODO: Make Message a type parameter to pass to the Pipeline class so we can
 * constrain the kinds of data flowing between workers as defined by the client
 * program. Ideally, Flow would be able to verify outputs of workers match the
 * expected input types of downstream workers.
 */
export type Message = any;

/**
 * WorkerError wraps classes deriving from Error that also describe behavior
 * tooqs.js can use to react to error situations.
 */
export interface WorkerError {
  retry(): boolean
};

/**
 * ConsumerError describes problems that arise when processing an incoming
 * message in a worker. The data pipeline can potentially retry a message based
 * on the retry behavior on the error instance.
 */
class ConsumerError extends Error {
  data: ?{};
  retry: boolean;

  /**
   * Creates a ConsumerError.
   *
   * @param {string} msg - The error message
   * @param {object} [data] - Any additional metadata about the error scenario useful
   * for logging and inspection.
   * @param {boolean} [retry=false] - Whether the message that produced this
   * error should be retried.
   */
  constructor(msg: string, data: ?{}, retry: ?boolean) {
    super(msg);

    if (retry == null) {
      this.retry = true;
    }

    this.data = data;
  }
}

/**
 * PublisherError describes problems that arise when attempting to publish a new
 * message to the pipeline. The data pipeline can potentially retry a message based
 * on the retry behavior on the error instance.
 *
 * Messages that produce a PublisherError are always retried.
 *
 * TODO Is this different enough from a ConsumerError to keep around?
 */
class PublisherError extends Error {
  data: ?{};
  retry: boolean;

  /**
   * Creates a PublisherError.
   *
   * @param {string} msg - The error message
   * @param {object} [data] - Any additional metadata about the error scenario useful
   * for logging and inspection.
   */
  constructor(msg: string, data: ?any) {
    super(msg);

    this.data = data;
    this.retry = true;
  }
}

/**
 * PublisherFn is a function that can submit a message to a pre-defined worker.
 * It is used by consumers to possibly forward messages on to the next worker in
 * the pipeline. Therefore, this function should be a closure over the correct
 * worker.
 *
 * TODO these types that define a worker are just bare functions for now. We'll
 * eventually want to model this as a classes implementing methods to satisfy
 * the interface.
 */
export type PublisherFn = Message => (?WorkerError)

/**
 * ConsumerFn is a function that can receive a message and do work on it, possibly
 * resulting in an error in the course of processing.
 */
export type ConsumerFn = Message => (?WorkerError)

/**
 * Worker is the primary atomic unit in a pipeline that can receive work and
 * possibly publish work to a predefined worker or set of workers.
 *
 * It is a function to a function that produces a function. This is to take
 * advantage of JavaScript closures to "inject" a PublisherFn pre-configured to
 * a specific worker or set of workers.
 *
 * For example, suppose a function called "publish" closes over a target worker:
 *
 *    const publish = msg => targetWorker(msg)
 *
 * This can be passed to a worker that will then return a consumer function
 * closed over the publish function. The ConsumerFn can just call "publish"
 * without needing to have any knowledge about where the message is going:
 *
 *    const consumer = publish => msg => {
 *      console.log(msg);
 *
 *      // Do some work on the message
 *
 *      // Forward the message on to the next worker via publisher
 *      publish(Object.assign(msg, { processed: true }));
 *    };
 *
 * With this strategy, we can de-couple workers knowing about each other or the
 * orchestration of the pipeline. This control is inverted to the Pipeline
 * object and tooqs.js keeps track of all the mapping.
 *
 * TODO Replace worker with a Class conforming to an interface. We still want to
 * be able to inject publishers.
 * TODO Use Result instead of Maybe
 */
export type Worker = PublisherFn => ConsumerFn

/**
 * Entry describes a worker or a set of workers that run in parallel within a
 * pipeline. This allows our registration function to distinguish each addition
 * to the pipeline.
 */
type Entry = Worker | Worker[];

/**
 * Pipeline orchestrates workers in a data pipeline and abstracts the work of
 * mapping target and receiver workers by injecting a publish function bound to
 * the appropriate worker or workers.
 *
 * Each pipeline defines a head named by a topic. This is useful for external
 * messaging systems like NSQ that don't have any knowledge of tooqs.js. However
 * it maintains a "head worker" to allow for programmatic publishing without
 * external knowledge of what worker is the head of a pipeline.
 *
 * Registration and orchestration of workers is achieved declaritively via the
 * register method.
 *
 * TODO Add generic type parameter to class to constrain behavior around
 * Topics and Messages.
 *
 * https://flow.org/en/docs/types/generics/
 */
class Pipeline {
  topic: string;
  head: ConsumerFn;

  // Maps a worker's name to its consumer implementation
  workers: { [string]: ConsumerFn }

  /**
   * Creates a Pipeline named for the topic. But it won't do much of anything!
   * Check out the register method next!
   *
   * TODO Given this constructor barely does anything, maybe register doesn't
   * need to exist and it can be part of the constructor.
   *
   * @param {string} topic - Names the pipeline and marks the topic for the head
   * of the pipeline.
   */
  constructor(topic : string) {
    this.topic = topic;
    this.workers = {};
  }

  /**
   * register adds workers to the pipeline and builds an internal map
   * orchestrating worker outputs to worker inputs.
   *
   * An entry to the pipeline can be:
   *
   * - Worker: a function that recieves a PublisherFn and returns a ConsumerFn
   *   that can process messages and optionally publish messages to the next
   *   entry in the pipeline
   * - Worker[]: a list of functions that define a pipeline step where the input
   *   message should be multiplexed across all of them.
   *
   * Some other notes to consider when building pipelines:
   *
   * - Data flows linearly through the pipeline: a Worker's output is routed to
   *   the Entry (a worker or set of parallel workers) following it in the
   *   entries list
   * - TBD: Document sub-pipelines and recursive patterns. Just have to build it
   *   first...
   * - TBD: Describe publishing to other pipelines with "string" type entries.
   * - The last entry in a pipeline is expected to terminate the pipeline
   *   without publishing on. A pipeline *can* recurse on itself, and in that
   *   case the worker should publish to a new sub-pipeline.
   *
   * @param [Entry[]] workers - a list of entries to register in order in the
   * pipeline.
   */
  register(...workers: Entry[]) : Pipeline {
    // loop recursively descends through the list of entries until it hits the
    // end. As it unrolls, it returns a publisher function to inject into the
    // worker before it. It also handles lists of parallel workers by wrapping
    // them in a publisher that dispatches a single message to each.
    const loop = (entries: Entry[]) : PublisherFn => {
      const [entry, ...rest] = entries;

      // At the end of the chain. Return a no-op publisher function and start
      // unrolling. We don't expect pipeline terminators to publish.
      if (entry == null) {
        // TODO publish function should throw a heckin error. Can't be trying to
        // publish at the end of the chain.
        return msg => null;
      }

      if (Array.isArray(entry)) {
        const fanIn = loop(rest);
        entry.forEach(worker => {
          // TODO a list of entries could possibly contain an entry that is
          // itself a list. That's not supported here and we only support
          // one-level of nested parallelism.
          this.workers[worker.name] = worker(fanIn)
        });

        // We return a wrapper function around the parallel workers to multiplex
        // the publishing.
        //
        // TODO Make this parallel just in case
        return msg => entry.forEach(worker => this.workers[worker.name](msg));
      } else {
        this.workers[entry.name] = entry(loop(rest));
        return this.workers[entry.name];
      }
    };

    const [head, ...rest] = workers;

    if (!(head && rest.length)) {
      // TODO better handling...I guess
      throw new Error('wtf is your problem. add workers...');
    }

    if (Array.isArray(head)) {
      const next = loop(rest);
      head.forEach(worker => {
        this.workers[worker.name] = worker(next)
      });

      this.head = msg => head.forEach(worker => this.workers[worker.name](msg));
    } else {
      this.head = head(loop(rest));
    }

    return this;
  }

  // [DEPRECATED]: This is an old implementation and I'm hanging onto it just in
  // case.
  register__reverseImplementation(...workers: Worker[]) : Pipeline {
    // We walk the chain of workers in reverse such that the preceding worker in
    // the chain is set up to publish to the current worker in the loop.
    // TODO this algorithm doc sucks...I need to explain this better
    for(let i = workers.length - 1; i >= 0; i--) {
      const worker = workers[i];
      const previous = workers[i - 1];

      if (!previous) {
        // We're at the front of the chain. The publish chain has already been
        // set up so we mark this as the head of the pipeline.
        if (this.head == null) {
          this.head = this.workers[worker.name];
        }
        break;
      }

      // Set the previous worker to publish to this worker
      const publisher = msg => {
        return this.workers[worker.name](msg);
      }

      this.workers[previous.name] = previous(publisher)
    }

    // The last worker in the chain is unique in that it is not expected to
    // publish any messages anywhere, so this is effectively a no-op. We'll throw
    // an error instead so clients know they tried to do something bad.
    const last = workers[workers.length - 1];
    if (last) {
      this.workers[last.name] = last(msg => {
        throw new PublisherError(
         'last worker in a pipeline attempted to publish' ,
          { worker: last },
        );
      });
    }

    return this;
  }

  /**
   * publish dispatches a message to the head of the pipeline
   *
   * @param {Message} msg - The message to dispatch to the pipeline.
   */
  publish(msg: Message) {
    if (this.head != null) {
      return this.head(msg);
    }

    // TODO throw error if the tip is undefined for some reason
  }
}

module.exports = {
  pipeline,
  ConsumerError,
  PublisherError
}
