const {
  Doubler,
  English,
  Evener,
  Italian,
  LinkParser,
  Logger,
  Printer,
  Receiver,
  Requester,
  Sanitizer,
  Spanish,
} = require('./lib/workers');

const tooqs = require('./lib/tooqs');

// Single-path pipeline
let pipeline = tooqs.
  pipeline('math').
  register(
    Evener,
    Doubler,
    Printer
  );

pipeline.publish([1, 2, 3, 4, 5]);

// Fan-out, fan-in pipeline
const translationPipeline = tooqs.pipeline('translator');

translationPipeline.
  register(
    Sanitizer,
    [
      English,
      Spanish,
      Italian,
    ],
    Printer
  );
translationPipeline.publish('hello here is something to translate');

/*
// Sub-pipelines
tooqs.
  pipeline('crawler').
  register(
    Requester,
    [
      // Note the recursive message
      pipeline('link-parser').
        register(
          LinkParser,
          'crawler'
        ),

      // Log in parallel
      pipeline('logger').register(Logger),
    ]
  )

// Batched messages
tooqs.
  pipeline('batched').
  register(
    tooqs.batch(Lines, () => boolean ),
    Joiner,
    Printer,
  )
*/
