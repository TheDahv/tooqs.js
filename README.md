# tooqs.js

> Totally Obvious Orchestration of Queueing Systems

This is a design proposal for a system to make it easier for us to build message
and queue processing systems.

## Problem Statement

But we should understand our problems first. What motivates us to find a better
way?

- Data pipelines are logical, implicit ideas--not explicit, first-class concepts
- Relationships among decoupled message processors are not explicit and
  difficult to discover
  - Worker graphs are declarative (defined in a JSON file for each worker) but not
    centralized
  - Message destinations from a worker are not explicit nor declarative; it
    requires grepping through code to discover what is downstream from a worker
- Consistency of published messages and expected input types can be difficult to
  achieve
  - Are we expecting a field called `id`? `objectId`? `object_id`???
- Entry points to a pipeline are not defined
  - Is there one safe way to start a pipeline? If not, do we know all the
    different places that can kick off a pipeline?
  - Can workers in the middle of the pipeline safely take arbitrary messages, or
    do they depend on work performed upstream?
  - Can a pipeline recurse on itself? Why? Where? How often?

## Goals

NSQ and projects against them offer advantages we want to keep:

- Small compute, error, and retry boundaries by spreading work across workers
- Fine-grained control of scale anywhere within the pipeline
- Decoupled and flexible designs

So what would help us design more obvious systems without sacrificing these
advantages? This proposes a "pipeline defining" framework whose design-goals
are:

- declarative: describe pipeline topologies with code
- expressive: describe common patterns with obvious and existing syntax and
  idioms:
  - fan-out, scaling, batching, throttling, etc.
- complete: we ought to be able to reproduce every pipeline flow that currently
  exists within existing implementations
- isolation of concerns: abstract worker connections from the workers to the
  framework
- implementation agnostic (bonus): we may use NSQ or Resque or RabbitMQ, but do
  our workers need to know that?
- consistent (bonus): proving the output of worker matches the expected input

## Proposal

Tooqs.js will expose a system for creating pipelines, registering workers, and
describing their orchestration in code.

All of these examples are in `demo.js`. Let's show a simple case: a single
linear pipeline with no branching or loops:

    tooqs.
      pipeline('my-pipeline').
      register(
        FirstWorker,
        SecondWorker,
        ThirdWorker
      )

In this case, we can clearly see:

- a pipeline and its entry point
- which workers are involved in the pipeline
- what order pipeline data follows as it flows through the workers

### Proposing Features and Moving Forward

This is a usage-driven design document. Patterns we would like to be able to
build--and the API we wish we had to describe them--are contained in `demo.js`.
As we identify more patterns and concepts we want to implement, we should add
example usage, agree on the syntax, and _then_ try to implement it.

The design should be demand-driven: features that aren't already described by
`demo.js` won't be implemented.

### Running and Contributing

Download the repo and run `npm install` to get started.

Some other commands you'll want:

- `npm run build` - Run Flow static analysis, and output pure JS stripped of
  Flow types to `lib`
- `node demo.js` - Runs the example files

If you want to add a workflow:

- Build out your workers as simple functions and export them in
  `src/workers/index.js`
- Import your worker and add a pipeline in `demo.js`

Don't worry about messing with `src/tooqs.js` for now as this implementation
isn't meant to be real but mostly just to stub just enough implementation to
support the design.

Focus on the _pipeline definition_ not the framework.

### wtf is Flow

> [FLOW](https://flow.org/) IS A STATIC TYPE CHECKER FOR JAVASCRIPT.

Flow is useful for designing a system by giving a programmer a means to express
ideas and plans through type and function annotations, and ensuring the
implementation enforces those ideas.

But really, Flow could be a useful tool to address a problem that exists in
large, decoupled systems architecture: human attention does not scale. But a
program can. Flow is a program that can read programs, and its static analysis
tools could assist programmers with knowing their implementations are correct
and consistent.

Moreover, code analysis tools should help us understand the impact of our
changes to complex data systems _before_ publishing them to production.

This proposal also includes Flow as a means to:

- build tooqs.js in a self-documenting and self-proving way
- expose a clear API with expectations and guarantees a client can program
  against
- exposed types a client can use to insure correctness and consistency within
  pipeline worker implementations
- provide all of this optionally

There are languages that transpile to JavaScript
([Reason](https://reasonml.github.io/),
[TypeScript](https://www.typescriptlang.org/), etc.) with stronger type systems.
However, Flow offers a gentler solution because:

- it is "just JavaScript" with additional annotations
- it is lightweight and fast
- it has great support with or without Babel (check out `package.json`. This
  proposal makes no use of Bablel or Webpack and we generate pure JS in the
  `lib` folder)
- lots of editor support
- it is totally optional

For this proposal, tooqs.js is implemented with Flow following [these
guidelines](https://github.com/ryyppy/flow-guide/tree/master/styleguide) and
using [JSDoc](http://usejsdoc.org/).

## Other Feature Ideas

- Programmatically generated pipeline documentation and diagrams 
- Automatic monitoring and metrics of pipelines
- Automatic scaling of pipelines

